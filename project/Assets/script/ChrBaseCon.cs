﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChrBaseCon : MonoBehaviour
{
  private ChrMoveMod m_MovementModel;
  private ChrIntMod m_InteractionModel;


  void Awake()
  {
    m_MovementModel = GetComponent<ChrMoveMod>();
    m_InteractionModel = GetComponent<ChrIntMod>();
  }

  protected void SetDirection( Vector2 direction )
  {
    if( m_MovementModel == null )
    {
      return;
    }

    m_MovementModel.SetDirection( direction );
  }

  protected void OnActionPressed()
  {
    if( m_InteractionModel == null )
    {
      return;
    }

    m_InteractionModel.OnInteract();
  }
}
