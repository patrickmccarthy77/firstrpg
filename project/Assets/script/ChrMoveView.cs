﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChrMoveView : MonoBehaviour
{
  public Animator Animator;

  private ChrMoveMod m_MovementModel;

    void Awake()
    {
      m_MovementModel = GetComponent<ChrMoveMod>();

      if( Animator == null )
      {
        Debug.LogError( "Character Animator not setup!" );
        enabled = false;
      }
    }

    void Update()
    {
      UpdateDirection();
    }

    void UpdateDirection()
    {
      Vector3 direction = m_MovementModel.GetDirection();

      if( direction != Vector3.zero )
      {
        Animator.SetFloat( "DirectionX", direction.x );
        Animator.SetFloat( "DirectionY", direction.y );
      }

      Animator.SetBool( "IsMoving", m_MovementModel.IsMoving() );
    }

}
