﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChrIntMod : MonoBehaviour
{
    private Collider2D m_Collider;
    private ChrMoveMod m_MovementModel;

    void Awake()
    {
      m_Collider = GetComponent<Collider2D>();
      m_MovementModel = GetComponent<ChrMoveMod>();
    }

    void Update()
    {

    }

    public void OnInteract()
    {
      Interactable usableInteractable = FindUsableInteractable();

      if( usableInteractable == null )
      {
        return;
      }

      Debug.Log( "Found Interactable:" + usableInteractable.name );
    }

    Interactable FindUsableInteractable()
    {
      Collider2D[] closeColliders = Physics2D.OverlapCircleAll( transform.position, 1f );
      Interactable closestInteractable = null;
      float angleToClosestInteractable = Mathf.Infinity;

      for( int i = 0; i < closeColliders.Length; ++ i )
      {
        Interactable colliderInteractable = closeColliders[ i ].GetComponent<Interactable>();

        if( colliderInteractable == null )
        {
          continue;
        }

        Vector3 directionToInteractable = closeColliders[ i ].transform.position - transform.position;

        float angleToInteractable = Vector3.Angle( m_MovementModel.GetFacingDirection(), directionToInteractable );

        if( angleToInteractable < 35 )
        {
          if( angleToInteractable < angleToClosestInteractable )
          {
            closestInteractable = colliderInteractable;
            angleToClosestInteractable = angleToInteractable;
          }
        }
      }
      return closestInteractable;
    }
}
